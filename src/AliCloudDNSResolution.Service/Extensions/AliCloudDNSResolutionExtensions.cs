using AliCloudDNSResolution.Application.Contract;
using AliCloudDNSResolution.Application.Contract.Modules;

namespace AliCloudDNSResolution.Application.Extensions;

/// <summary>
/// 阿里云解析DNS方法注入
/// </summary>
public static class AliCloudDNSResolutionExtensions
{
    public static IServiceCollection AddAliCloudDNSResolution(this IServiceCollection services,
        IConfiguration configuration)
    {
        services.Configure<AliCloudAccessOptions>(configuration.GetSection(AliCloudAccessOptions.Name));
        services.Configure<UpdateDomainRecordInput>(configuration.GetSection(UpdateDomainRecordInput.Name));
        services.AddSingleton<HttpClient>();
        services.AddSingleton<AddrIpService>();
        services.AddSingleton<DnsResolutionService>();
        services.AddHostedService<AliCloudDnsResolutionWorker>();
        return services;
    }
}