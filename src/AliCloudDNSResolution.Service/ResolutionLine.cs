namespace AliCloudDNSResolution.Application.Contract;

public enum ResolutionLine
{
    /// <summary>
    /// 默认
    /// </summary>
    Default,
    
    /// <summary>
    /// 电信
    /// </summary>
    Telecom,
    
    /// <summary>
    /// 联通
    /// </summary>
    Unicom,
    
    /// <summary>
    /// 移动
    /// </summary>
    Mobile,
    
    /// <summary>
    /// 海外
    /// </summary>
    Oversea,
    
    /// <summary>
    /// 教育网
    /// </summary>
    Edu,
    
    /// <summary>
    /// 鹏博士
    /// </summary>
    Drpeng,
    
    /// <summary>
    /// 广电网
    /// </summary>
    Btvn
}