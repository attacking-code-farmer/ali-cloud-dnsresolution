using AliCloudDNSResolution.Application.Extensions;

IHost host = Host.CreateDefaultBuilder(args)
    .ConfigureServices((context,services) =>
    {
        services.AddAliCloudDNSResolution(context.Configuration);
    })
    .Build();

await host.RunAsync();