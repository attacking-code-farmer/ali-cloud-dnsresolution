using Newtonsoft.Json;

namespace AliCloudDNSResolution.Application;

public class AddrIpService 
{
    private readonly HttpClient _client;

    public AddrIpService(HttpClient client)
    {
        _client = client;
    }

    public async Task<string> GetAddressIpAsync()
    {
        var result = await _client.GetAsync("https://ident.me/.json");

        var message = await result.Content.ReadAsStringAsync();

        var json = JsonConvert.DeserializeObject<dynamic>(message);
    
        return json.address;
    }
}