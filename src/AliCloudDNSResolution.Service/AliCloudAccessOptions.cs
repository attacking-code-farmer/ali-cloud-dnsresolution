﻿namespace AliCloudDNSResolution.Application.Contract;

/// <summary>
/// 阿里云Access账号配置
/// </summary>
public class AliCloudAccessOptions
{
    public const string Name = "AliCloud";
    
    /// <summary>
    /// AccessKey
    /// </summary>
    public string? AccessKey { get; set; }
    
    /// <summary>
    /// AccessKeySecret
    /// </summary>
    public string? AccessKeySecret { get; set; }

    /// <summary>
    /// 解析的域名
    /// </summary>
    public string? DomainName { get; set; }

    /// <summary>
    /// 任务定时时间(分钟)
    /// </summary>
    public int TaskTime { get; set; } = 120;
}