namespace AliCloudDNSResolution.Application.Contract.Modules;

public class UpdateDomainRecordInput
{
    public const string Name = "UpdateDomainRecord";
    
    /// <summary>
    /// 语言
    /// </summary>
    public string? Lang { get; set; }

    /// <summary>
    /// 用户端ip
    /// </summary>
    public string? UserClientIp { get; set; }

    /// <summary>
    /// 主机记录。
    /// </summary>
    public string? RR { get; set; }
    
    /// <summary>
    /// 解析记录的id
    /// </summary>
    public string? RecordId { get; set; }

    /// <summary>
    /// 解析记录的类型
    /// </summary>
    public string? Type { get; set; }

    /// <summary>
    /// 解析ip
    /// </summary>
    public string? Value { get; set; }

    /// <summary>
    /// 生效时间
    /// </summary>
    public int Ttl { get; set; } = 600;

    /// <summary>
    /// MX记录的优先级
    /// </summary>
    public int? Priority { get; set; }

    /// <summary>
    /// 解析线路
    /// </summary>
    public ResolutionLine Line { get; set; } = ResolutionLine.Default;
}